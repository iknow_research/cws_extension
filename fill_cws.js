(function(){
  var dontLogMeOut = document.createElement('script');
  dontLogMeOut.innerHTML = `
  window.CloseSessionOut.closeSessionOutForOthers = () => {};
  window.CloseSessionOut.closeSessionOutForIE = () => {};`

  document.body.appendChild(dontLogMeOut);

  var nextButtonOnDayList = document.querySelector('.mg_normal + .mg_normal input[value="詳細"]');
  var confirmChangesButton = document.getElementById('btnNext0');
  var approveChangesButton = document.getElementById('dSubmission0');

  if (nextButtonOnDayList) {
    click(nextButtonOnDayList);
    return;
  } else if (approveChangesButton) {
    click(approveChangesButton);
  } else  {
    fillDetail();

    if (confirmChangesButton) {
      click(confirmChangesButton);
    }
  }

  function click (element) {
    element.focus();
    element.dispatchEvent(new Event('click'));
  }

  function fillDetail () {
    function pickSelectElement(nodeName, targetValue) {
      var select = document.evaluate("//select[@name='" + nodeName + "']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
      var option = document.evaluate("option[text()='" + targetValue + "']", select, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
      select.value = option.value;
      select.focus();
      select.dispatchEvent(new Event('change'));
      return [select, option];
    }

    function matchesError(errorText) {
      return document.evaluate("count(//span[@class='error'][contains(text(), '" + errorText + "')]) > 0",
                               document, null, XPathResult.BOOLEAN, null).booleanValue;
    }

    // Pick combo boxes first
    if(matchesError('始業 打刻がありません')){
      pickSelectElement('SGYCD1S', 'その他');
    }

    if(matchesError('終業 打刻がありません')){
      pickSelectElement('SGYCD2S', 'その他');
    }

    if(matchesError('遅刻理由')) {
      pickSelectElement('GI_COMBOBOX915_Seq0S', '私用');
    }

    if(matchesError('残業理由')) {
      pickSelectElement('GI_COMBOBOX920_Seq0S', '業務都合');
    }

    if(matchesError('早退理由')) {
      pickSelectElement('GI_COMBOBOX916_Seq0S', '私用');
    }

    if(matchesError('早出理由')) {
      pickSelectElement('GI_COMBOBOX919_Seq0S', '業務都合');
    }

    var time_context = document.evaluate("//table[@id='KNM']//td[@class='kinmu_normal'][last()]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    if(!time_context){
      return;
    }

    var interval = 15 * 60000; // 15 minutes

    function parseTimeNodes(nodes){
      var h = parseInt(nodes.snapshotItem(0).textContent);
      var m = parseInt(nodes.snapshotItem(1).textContent);
      return new Date(2000, 1, 1, h, m, 0, 0);
    }

    var start_time_nodes = document.evaluate("span[@id = 'H' or @id = 'M'][following-sibling::node()[contains(., '--')]]/text()", time_context, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    if(start_time_nodes && start_time_nodes.snapshotLength == 2){
      var start_time = parseTimeNodes(start_time_nodes);

      // Round start time up
      var start_offset = start_time.getTime() % interval;
      if(start_offset > 0) {
        start_time = new Date(start_time.getTime() + (interval - start_offset));
      }

      // and set the time fields
      document.getElementById("KNMTMRNGSTH").value = start_time.getHours();
      document.getElementById("KNMTMRNGSTM").value = start_time.getMinutes();
    }

    var end_time_nodes = document.evaluate("span[@id = 'H' or @id = 'M'][preceding-sibling::node()[contains(., '--')]]/text()", time_context, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);

    if(end_time_nodes && end_time_nodes.snapshotLength == 2){
      var end_time = parseTimeNodes(end_time_nodes);

      // and end time down
      var end_offset = end_time.getTime() % interval;
      if(end_offset > 0) {
        end_time = new Date(end_time.getTime() - end_offset);
      }

      document.getElementById("KNMTMRNGETH").value = end_time.getHours();
      document.getElementById("KNMTMRNGETM").value = end_time.getMinutes();
    }
  }
})();
