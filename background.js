chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    var url = tabs[0].url;
    var domain = url.match(/^[\w-]+:\/*\[?([\w\.:-]+)\]?(?::\d+)?/)[1];

    if(domain === "cws.kcgrp.jp"){
      // Call the script
      chrome.tabs.executeScript({ file: 'fill_cws.js' });
    }
    else{
      // open new CWS tab
      chrome.tabs.create({ url: "https://cws.kcgrp.jp/cws30/cws?isFromCWS=true&@SN=root.cws.cwslauncher" });
    }
  });
});
